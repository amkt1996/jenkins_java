multibranchPipelineJob('UAT_JENKINS_JAVA_BUILD') {
   branchSources {
       git {
           remote('git@gitlab.com:amkt1996/jenkins_java.git')
        }
        configure {
            it / factory(class: 'org.jenkinsci.plugins.workflow.multibranch.WorkflowBranchProjectFactory') {
               owner(class: 'org.jenkinsci.plugins.workflow.multibranch.WorkflowMultiBranchProject', reference: '../..')
              scriptPath('jenkins/build/buildrootJenkinsfile.uat')
            }
       }
   }
}